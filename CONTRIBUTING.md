**Guide for contributing**  
Important things to remember  
1. The bot is written in Discord.py **asyncio**.  
2. All new commands should be in a seperate cog, `/cogs/foo.py` and be automatically imported by the bot on runtime.  
3. Make sure the code actually works before submitting.  
That's it, enjoy submitting.  